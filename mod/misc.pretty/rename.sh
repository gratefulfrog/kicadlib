#!/bin/bash
# Rename all *.mod to *.kicad_mod
for f in *.mod; do 
mv -- "$f" "${f%.mode}.kicad_mod"
done
